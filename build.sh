#!/usr/bin/env bash
set -e # stop processing if something fails
set -u # error if an undefined variable is used
set -x # write to standard error a trace for each command after it expands
cd "$(dirname "$0")" # set current working directory to the directory of the script

# Build image
docker build \
    --tag jeffs-process-audio:local \
    ./images/jeffs-process-audio

# Vehicle
docker run \
    --mount "type=bind,source=${PWD}/links_jamendo.txt,target=/process/batch-file.txt,readonly" \
    --mount "type=bind,source=${PWD}/links_jamendo_downloaded.txt,target=/process/download-archive.txt" \
    --mount "type=bind,source=${PWD}/ogg_jamendo,target=/process/ogg" \
    jeffs-process-audio:local \
    /process/download.sh
mkdir -p pk3_jamendo
docker run \
    --mount "type=bind,source=${PWD}/ogg_jamendo,target=/process/ogg,readonly" \
    --mount "type=bind,source=${PWD}/listing_jamendo.txt,target=/process/listing.txt" \
    --mount "type=bind,source=${PWD}/pk3_jamendo,target=/process/pk3" \
    jeffs-process-audio:local \
    /process/generate.sh
cat listing_untracked.txt listing_jamendo.txt > listing_vehicle.txt

# Nexuiz
mkdir -p ogg_nexuiz
docker run \
    --mount "type=bind,source=${PWD}/ogg_nexuiz_source,target=/process/ogg_source,readonly" \
    --mount "type=bind,source=${PWD}/ogg_nexuiz,target=/process/ogg_target" \
    jeffs-process-audio:local \
    /process/normalize.sh
mkdir -p pk3_nexuiz
docker run \
    --mount "type=bind,source=${PWD}/ogg_nexuiz,target=/process/ogg,readonly" \
    --mount "type=bind,source=${PWD}/listing_nexuiz.txt,target=/process/listing.txt" \
    --mount "type=bind,source=${PWD}/pk3_nexuiz,target=/process/pk3" \
    jeffs-process-audio:local \
    /process/generate.sh
