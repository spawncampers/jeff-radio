#!/usr/bin/env bash
set -e # stop processing if something fails
set -u # error if an undefined variable is used
set -x # write to standard error a trace for each command after it expands
cd "$(dirname "$0")" # set current working directory to the directory of the script

while IFS= read -r -d $'\0' LINE; do
  BASENAME=$(basename -- "$LINE")
  IN="ogg_source/$BASENAME"
  OUT="ogg_target/$BASENAME"
  [[ ! -f "$OUT" ]] && ffmpeg -hide_banner -loglevel panic -y -i "$IN" -filter:a loudnorm -filter:a "volume=2" "$OUT" < /dev/null
done < <(find ogg_source -type f -print0)
