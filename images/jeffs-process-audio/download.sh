#!/usr/bin/env bash
set -e # stop processing if something fails
set -u # error if an undefined variable is used
set -x # write to standard error a trace for each command after it expands
cd "$(dirname "$0")" # set current working directory to the directory of the script

pushd ogg
youtube-dl \
  --format ogg \
  --continue \
  --no-overwrites \
  --ignore-errors \
  --download-archive ../download-archive.txt \
  --batch-file ../batch-file.txt || true
popd
