#!/usr/bin/env bash
set -e # stop processing if something fails
set -u # error if an undefined variable is used
set -x # write to standard error a trace for each command after it expands
cd "$(dirname "$0")" # set current working directory to the directory of the script

./node_modules/.bin/xon-radio-generate-list \
  --input-songs 'ogg/*.ogg' \
  --output-package-directory pk3 \
  --url-prefix 'http://jeffs.eu/radio/' \
  --output-list-file listing.txt
