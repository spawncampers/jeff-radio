## Radio Vehicle:

1. `links_jamendo.txt` (in repo) ->
2. `ogg_jamendo/*.ogg` (in repo) and `links_jamendo_downloaded.txt` (in repo) ->
3. `pk3_jamendo/*.pk3` (generated) and `listing_jamendo.txt` (in repo)
4. `listing_untracked.txt` (in repo) + `listing_jamendo.txt` (in repo) = `listing_vehicle.txt` (in repo)

## Radio Nexuiz:

1. `ogg_nexuiz_source/*.ogg` (in repo) ->
2. `ogg_nexuiz/*.ogg` (generated) ->
3. `pk3_nexuiz/*.pk3` (generated) and `listing_nexuiz.txt` (in repo)

## Usage:

Requires **Docker**.

```bash
./build.sh
```

## To sync with the server (upload), run:

```bash
./upload-to-server.sh
```
