#!/usr/bin/env bash
set -e # stop processing if something fails
set -u # error if an undefined variable is used
set -x # write to standard error a trace for each command after it expands
cd "$(dirname "$0")" # set current working directory to the directory of the script

echo 'Installing radio Vehicle/Resurrection...'
rsync --archive --itemize-changes --size-only listing_vehicle.txt game1:/home/jeff/.radio_resurrection.txt
rsync --archive --itemize-changes --size-only pk3_jamendo/ game1:/home/jeff/.radio_pk3/
rsync --archive --itemize-changes --size-only pk3_untracked/ game1:/home/jeff/.radio_pk3/

echo 'Installing radio Nexuiz...'
rsync --archive --itemize-changes --size-only listing_nexuiz.txt game1:/home/jeff/.radio_nexuiz.txt
rsync --archive --itemize-changes --size-only pk3_nexuiz/ game1:/home/jeff/.radio_pk3/
